/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hbrs.ooka.demo.json;

/**
 *
 * @author Micha
 */
public class TwitterTweet {
  
  public String text;
  public String created_at;
  
  public String toString(){
    return "Posted "+text+" at "+created_at;
  }  
  
}
