package de.hbrs.ooka.demo.json;

import de.hbrs.ooka.ejb.restservice.SimpleJsonParser;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Micha
 */

@WebServlet(name = "JsonDemoTwitterServlet", urlPatterns = {"/JsonDemoTwitterServlet"})
public class JsonDemoTwitterServlet extends HttpServlet {
 
  @EJB
  private SimpleJsonParser SimpleParser;
  

  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
   
   
    // ----------------------------------------
    // Fetch TwitterUsers from local JSON-File which is located @ WebPages-Root\twitterUser.json
    // JSON File contains Data from users "TwitterAPI" and "sanaulla"
    // ----------------------------------------
    
    String localJsonSource  = "twitterUser.json";
    String localJsonPath    = this.getServletContext().getRealPath(localJsonSource);

    this.SimpleParser.setResultClassType(TwitterUser.class);
    List<TwitterUser> twitterUsersFromFile  = SimpleParser.fetchLocalJsonContent(localJsonPath);
    
    // ----------------------------------------
    // A TwitterUser from remote Twitter-API
    // https://api.twitter.com/1/users/show.json?screen_name=TwitterAPI
    // Update: This example does not work any more cause of new Twitter-API 1.1 !!!
    // Build your own solution if you are interested in a working demo
    // ----------------------------------------
        
    String remoteJsonSource               = "https://api.twitter.com/1/users/show.json?screen_name=saschalobo";
    //List<TwitterUser> twitterUserFromWeb  = SimpleParser.fetchRemoteJsonContent(remoteJsonSource);
    
    // ----------------------------------------
    // Some Tweets from remote Twitter-API
    // http://api.twitter.com/1/statuses/user_timeline.format
    // Update: This example does not work any more cause of new Twitter-API 1.1 !!!
    // Build your own solution if you are interested in a working demo    
    // ----------------------------------------
    
    String remoteFetchUrl               = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=saschalobo";

    this.SimpleParser.setResultClassType(TwitterTweet.class);
    //List<TwitterTweet> twitterTweets    = SimpleParser.fetchRemoteJsonContent(remoteFetchUrl);
    
    // ----------------------------------------
    
    try {

      out.println("<html>");
      out.println("<head>");
      out.println("<title>JsonDemoTwitterServlet</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet JsonDemoTwitterServlet at " + request.getContextPath() + "</h1>");
      out.println("This Servlet shows you how to use GSON for parsing local JSON-Files and processing ");
      out.println("JSON retrieved from remote JSON-based API's like Twitter via a simple URL-Connection");
      out.println("<hr><br>");
      
            
      out.println("<h2>TwitterUsers from local JSON-File</h2>");
      for( TwitterUser user : twitterUsersFromFile ) {
        out.println(user);
        out.println("<br>");
      } 
      
      out.println("<hr>");
      out.println("<h3 style='color:red;'>Sorry remote examples won't work any more cause of changed Twitter-API and restricted access</h3>");
      out.println("<hr>");

      out.println("<h2>A TwitterUser from remote Twitter-API</h2>");
//      for( TwitterUser user : twitterUserFromWeb) {
//        out.println(user);
//        out.println("<br>");
//      }       
      out.println("<hr>");

      out.println("<h2>TwitterTweets from remote Twitter-API</h2>");
//      for( TwitterTweet tweet : twitterTweets ) {
//        out.println(tweet);
//        out.println("<br>");
//      } 
      out.println("<hr>");
      
      
      out.println("</body>");
      out.println("</html>");
    } finally {      
      out.close();
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP
   * <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP
   * <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>
}
